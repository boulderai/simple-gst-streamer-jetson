#!/bin/bash

sudo apt-get update && \
    sudo apt-get install -y \
    libgstreamer1.0-dev \
    libgstreamer-plugins-base1.0-dev \
    git \
    gtk-doc-tools

git submodule update --init
cd gst-rtsp-server && \
./autogen.sh && \
make

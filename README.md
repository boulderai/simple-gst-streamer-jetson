# Overview

This repository includes scripts to install dependencies,
build and run a gstreamer based rtsp streamer from the
project at https://github.com/GStreamer/gst-rtsp-server.git

## Building

Run `./build.sh`

## Running

Start the stream with `./rtsp-stream.sh`.  Then open the rtsp
stream at the URL printed on the terminal.

